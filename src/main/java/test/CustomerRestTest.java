package test;

import model.CreateCustomerRequest;
import model.CustomerResponse;
import model.UpdateCustomerRequest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

public class CustomerRestTest {

    private static final String customersUri;
    private static final String customersByIdUri;
    private static final RestTemplate restTemplate;

    static {
        restTemplate = new RestTemplate();
        customersUri = UriComponentsBuilder.newInstance()
                .scheme("http")
                .host("localhost")
                .port(8080)
                .pathSegment("api", "customers")
                .build()
                .toUriString();
        customersByIdUri = UriComponentsBuilder.newInstance()
                .scheme("http")
                .host("localhost")
                .port(8080)
                .pathSegment("api", "customers", "{id}")
                .build()
                .toUriString();


    }

    private static List<CustomerResponse> getAllCustomers() {
        return restTemplate.exchange(customersUri,
                                     HttpMethod.GET,
                                     null,
                                     new ParameterizedTypeReference<List<CustomerResponse>>() {})
                                     .getBody();
    }

    private static CustomerResponse getById(long id) {
        return restTemplate.getForObject(customersByIdUri, CustomerResponse.class, id);
    }

    private static void createCustomer(String firstName, String lastName) {
        CreateCustomerRequest request = new CreateCustomerRequest(firstName, lastName);
        restTemplate.postForObject(customersUri, request, Void.class);
    }


    private static void updateCustomer(long id, UpdateCustomerRequest request) {
        restTemplate.put(customersByIdUri, request, id);
    }

    private static void deleteCustomer(long id) {
        restTemplate.delete(customersByIdUri, id);
    }

}
